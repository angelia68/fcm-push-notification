package com.example.fcmpushnotification.firebasenotification.dto

data class PushNotificationDTO(
    val data: NotificationDataDTO,
    val to: String
)