package com.example.fcmpushnotification.firebasenotification.dto


data class NotificationDataDTO(
    val title: String,
    val message: String
)