package com.example.fcmpushnotification

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.fcmpushnotification.firebasenotification.FCMService
import com.example.fcmpushnotification.firebasenotification.dto.NotificationDataDTO
import com.example.fcmpushnotification.firebasenotification.dto.PushNotificationDTO
import com.example.fcmpushnotification.firebasenotification.RetrofitInstance
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


const val TOPIC = "/topics/myTopic"

class MainActivity : AppCompatActivity(), View.OnClickListener {
    val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getFirebaseInstanceId()
        initListener()
    }

    private fun sendNotification(notification: PushNotificationDTO) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val response = RetrofitInstance.api.postNotification(notification)
            if(response.isSuccessful){
                Log.d("<TEST1>", "Response: ${Gson().toJson(response)}")
            }
            else{
                Log.e("<TEST>", response.errorBody()!!.string())
            }
        }
        catch (e: Exception){
            Log.e(TAG, e.toString())
        }
    }

    private fun getFirebaseInstanceId(){
        FCMService.sharedPref = getSharedPreferences("sharedPref", Context.MODE_PRIVATE)

        FirebaseInstanceId.getInstance().instanceId
            .addOnSuccessListener {
                FCMService.token = it.token
                tv_current_device_id.text = it.token
                Log.d("<TEST>", "Token: " + it.token)
            }

        FirebaseMessaging.getInstance().subscribeToTopic(TOPIC)
    }

    private fun initListener(){
        btn_send_notification.setOnClickListener(this)
        ib_copy.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            if(v == btn_send_notification){
                val title = "New Notification!"
                val message = et_message.text.toString()
                var recipientToken = et_receiver_device_id.text.toString()
                if(recipientToken.isNullOrEmpty()) recipientToken = TOPIC   //to send notification that has the same topic
                if(title.isNotEmpty() && message.isNotEmpty() && recipientToken.isNotEmpty()){
                    PushNotificationDTO(NotificationDataDTO(title, message), recipientToken).also {
                        sendNotification(it)
                    }
                }
            }
            else if(v == ib_copy){
                val clipboard: ClipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clip: ClipData = ClipData.newPlainText("text", tv_current_device_id.text.toString())
                clipboard.setPrimaryClip(clip)
                Toast.makeText(this, "Token Copied!", Toast.LENGTH_SHORT).show()
            }
        }
    }
}